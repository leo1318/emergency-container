#!/bin/bash
source ./files.conf

helpMessage() {
    echo "./run.sh --password PASSWORD [--containername NAME]"
}

testMandatoryParameters() {
    if [ "$password" == "" ]
    then
        echo "You have to set --password"
        helpMessage
        exit 255
    fi
    
}

createVeracryptContainer() {
    echo "Start veracrypt container creation"
    veracrypt -t -c ./build/$path --encryption=AES --filesystem=FAT --hash=SHA-512 -p "$password" --random-source=/dev/random --size=100M --volume-type=normal -k "" --pim=0
    echo "Creation done"
}

mountVeracryptContainer() {
    echo "Start mounting container"
    veracrypt --mount ./build/$path /media/veracrypt1  -p "$password"
    echo "Container was mounted"
}

unmountVeracryptContainer() {
    echo "Unmount Container"
    veracrypt -d
}

copyFiles() {
    echo "Start copy"
    for var in "${copyfiles[@]}"
    do
        cp -r $var /media/veracrypt1
        echo "Copied $var"
    done
    echo "Done copy"
}

copyContainer() {
    echo "copy container to "
    for var in "${copycontainerto[@]}"
    do
        cp -r ./build/$path.zip "$var"
        echo "Copied container to $var"
    done
    echo "Done copy container to"
}

zipAll() {
     zip -p ./build/$path.zip ./build/*
}

clearBuid() {
    echo "Clear build folder"
    rm -r ./build/*
}

downloadFiles() {
    echo "download files to "
    for var in "${downloadfiles[@]}"
    do
        wget -P ./build "$var"
        echo "Download container to $var"
    done
    echo "Done download files"
}

#Variables
password=""
path="container"

saveNext=""
for var in "$@"
do
    if [ "$saveNext" == "" ]
    then
        case  "$var" in
            "--password" ) saveNext="password"
            ;;
            "--containername" ) saveNext="path"
            ;;
            * ) echo "Unknown parameter $var \n"
                helpMessage
            ;;
        esac
    else
        case "$saveNext" in
            "password" ) password=$var
            ;;
            "path" ) path=$var
            ;;
        esac
        saveNext=""
    fi
    
done

testMandatoryParameters
createVeracryptContainer
mountVeracryptContainer
copyFiles
unmountVeracryptContainer
downloadFiles
zipAll
copyContainer
clearBuid
echo "Finished?"


